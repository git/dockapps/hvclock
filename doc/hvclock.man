.TH HVCLOCK 1 "October 2010" "hvclock" "User's Manual"

.SH NAME
hvclock \- Dockable analog clock and calendar

.SH SYNOPSIS
.B hvclock
[\fIOPTION\fR]...

.SH DESCRIPTION
\fBhvclock\fR is a dockable, anti-aliased, analog clock and calendar
application for the WindowMaker window manager.

The interface is kept very simple. To switch between calendar view and
clock view, single-click on the clock or calendar image.

.SH "OPTIONS"

.TP
\fB\-d\fR
display debugging messages.

.TP
\fB\-h\fR
display usage and exit

.TP
\fB\-s\fR
display seconds hand

.TP
\fB\-v\fR
output version information and exit

.SH CREDITS
\fBhvclock\fR was written by Hugo Villeneuve <hugo@hugovil.com>.

.SH COPYRIGHT
\fBhvclock\fR is free; anyone may redistribute it to anyone under the terms
stated in the GNU General Public License. A copy of the license is included in
the \fBhvclock\fR distribution. You can also browse it online at
.I http://www.gnu.org/copyleft/gpl.html
