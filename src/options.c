/*
 * options.c -- functions for processing command-line options and arguments
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

/* For proper scope */
#define OPTIONS_M 1

#if HAVE_CONFIG_H
#  include "config.h"
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#if STDC_HEADERS
#  include <string.h>
#elif HAVE_STRINGS_H
#  include <strings.h>
#endif

#include "common.h"
#include "hvclock.h"
#include "options.h"

#define PACKAGE_DESCRIPTION "Dockable analog clock and calendar"

static const char *opt_string = "vh?ds";

static const struct option long_opts[] = {
    {"help",         no_argument,       NULL, 'h'},
    {"version",      no_argument,       NULL, 'v'},
    {"debug",        no_argument,       NULL, 'd'},
    {"seconds",      no_argument,       NULL, 's'},
    {NULL,           no_argument,       NULL, 0}
};

/*
 * Display the help message
 */
static void
display_help(void)
{
    printf("%s\n\n", PACKAGE_NAME " - " PACKAGE_DESCRIPTION);
    printf("Usage: %s [OPTION]...\n", PACKAGE_NAME);
    printf("  -h, --help         Display this help and exit\n");
    printf("  -v, --version      Display version information and exit\n");
    printf("  -d, --debug        Display debugging messages\n");
    printf("  -s, --seconds      Display seconds hand\n");
    printf("\n");
}

/*
 * Display version information and exit
 */
static void
display_version(void)
{
    printf(PACKAGE_STRING);
    printf("\n");
}

/*******************************************************************************
 * Initializes the different options passed as arguments on the command line.
 ******************************************************************************/
void
parse_command_line_options(int argc, char *argv[])
{
    int opt;
    int long_index = 0;

    /*
     * Setting default values.
     *
     * You don't ordinarily need to copy an option string, since it is a
     * pointer into the original argv array, not into a static area that might
     * be overwritten.
     */
    opts.debug = false;
    opts.show_seconds = false;

    /* Parse our arguments. */
    opt = getopt_long(argc, argv, opt_string, long_opts, &long_index);

    while (opt != -1)
    {
        switch (opt)
        {
        case 'd':
            opts.debug = true;
            break;
        case 's':
            opts.show_seconds = true;
            break;
        case 'h':
        case '?':
            display_help();
            exit(EXIT_SUCCESS);
            break;
        case 'v':
            display_version();
            exit(EXIT_SUCCESS);
            break;
        default:
            /* You won't actually get here. */
            break;
        }

        opt = getopt_long(argc, argv, opt_string, long_opts, &long_index);
    }
}
