/*
 * hvclock.h
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef HVCLOCK_H
#define HVCLOCK_H 1

struct hvclock_t {
	int debug;
	int show_seconds;
};

/* Exported variables */
#undef _SCOPE_
#ifdef HVCLOCK_M
#  define _SCOPE_ /**/
#else
#  define _SCOPE_ extern
#endif

_SCOPE_ struct hvclock_t hvclock_infos;

#endif /* HVCLOCK_H */
