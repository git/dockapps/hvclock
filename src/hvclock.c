/*
 * hvclock.c -- main program
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#if HAVE_CONFIG_H
#  include "config.h"
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>

#include "common.h"
#include "options.h"
#include "dockapp.h"
#include "clock.h"
#include "hvclock.h"

void ErrorLocation(const char *file, int line)
{
	fprintf(stderr, "  Error in file \"%s\" at line #%d\n", file, line);
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int main(int argc, char *argv[])
{
	/* Initialization */
	parse_command_line_options(argc, argv);

	/* Initializing and creating a DockApp window. */
	dockapp_init(argc, argv);

	gtk_main();

	return EXIT_SUCCESS;
}
