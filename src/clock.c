/*
 * clock.c -- functions for displaying and rendering clock and calendar
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on EggClockFace
 * (c) 2005, Davyd Madeley <davyd@madeley.id.au>
 *
 * This file is released under the GPLv2
 */

/* Define filename_M */
#define CLOCK_M 1

#if HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#if STDC_HEADERS
#  include <string.h>
#elif HAVE_STRINGS_H
#  include <strings.h>
#else
#  error "Needs <string.h> or <strings.h>"
#endif
#include <math.h>
#include <time.h>
#include <sys/types.h>
#include <gtk/gtk.h>

#include "common.h"
#include "options.h"
#include "clock.h"
#include "hvclock.h"
#include "dockapp.h"
#include "clock-mask.xpm"
#include "calendar-mask.xpm"

/* Constants */
#define CLOCK_MODE    0
#define CALENDAR_MODE 1

/* All ratios are relatives to clock diameter */

#define HAND_HOURS_LENGTH_RATIO    0.55
#define HAND_MINUTES_LENGTH_RATIO  0.80
#define HAND_SECONDS_LENGTH1_RATIO 0.80 /* Main hand */
#define HAND_SECONDS_LENGTH2_RATIO 0.25 /* Small extension on opposite side */

#define HAND_HOURS_WIDTH_RATIO   0.100
#define HAND_MINUTES_WIDTH_RATIO 0.070
#define HAND_SECONDS_WIDTH_RATIO 0.040

#define CENTER_CIRCLE_OUTER_RADIUS_RATIO 0.100
#define CENTER_CIRCLE_INNER_RADIUS_RATIO 0.030

#define CLOCK_DIGITS_FONTS_SIZE_RATIO 0.30

/* Manual tweak for '11' oclock position */
#define ELEVENTH_OCLOCK_FIRST_DIGIT_XOFFSET_RATIO 0.04
#define TENTH_OCLOCK_FIRST_DIGIT_XOFFSET_RATIO    0.04

/* Offset between digits of '10', '11' and '12' oclock positions */
#define SECOND_DIGIT_XOFFSET 1.0

#define DIGITS_POSITION_RATIO      0.73 /* Position of digits from center */
#define DIGITS_DOTS_POSITION_RATIO 0.92 /* Position of small hours dots */

/* Radius of small dots near each digit */
#define DIGITS_DOTS_RADIUS_RATIO 0.04

#define CALENDAR_TEXT_FONTS_SIZE_RATIO   0.15
#define CALENDAR_DIGITS_FONTS_SIZE_RATIO (3.5 * CALENDAR_TEXT_FONTS_SIZE_RATIO)

#define CALENDAR_WEEKDAY_Y_POSITION_RATIO    0.20
#define CALENDAR_MONTH_Y_POSITION_RATIO      0.40
#define CALENDAR_DAYOFMONTH_Y_POSITION_RATIO 0.92

/* Starting mode is clock mode */
static int hvclock_mode = CLOCK_MODE;

#if defined (HAVE_GTK2)
/* In preparation for Gtk+-3 support. */
#define gtk_widget_get_allocated_width(_widget_) _widget_->allocation.width
#define gtk_widget_get_allocated_height(_widget_) _widget_->allocation.height
#endif

static double
get_clock_face_radius(GtkWidget *clock)
{
	/* Compute clock face radius */
	return (MIN(gtk_widget_get_allocated_width(clock), gtk_widget_get_allocated_height(clock)) / 2) - 3;
}

static struct tm *
clock_get_time(void)
{
	time_t now; /* Current system time */

	/* Get the current time */
	now = time(NULL);

	/* Format and print the time, "ddd yyyy-mm-dd hh:mm:ss zzz" */
	return localtime(&now);
}

static void
draw_clock_background(GtkWidget *clock, cairo_t *cr)
{
	gint center_x, center_y;
	double angle = M_PI / 3; /* Starting angle at '12' oclock position */
	double radius, radius_numbers, radius_dots;
	int digit;

	center_x = gtk_widget_get_allocated_width(clock) / 2;
	center_y = gtk_widget_get_allocated_height(clock) / 2;

	radius = get_clock_face_radius(clock);

	/* Position of digits as a radius ratio */
	radius_numbers = radius * DIGITS_POSITION_RATIO;

	/* Position of small hours dots as a radius ratio */
	radius_dots = radius * DIGITS_DOTS_POSITION_RATIO;

	cairo_save(cr);

	cairo_select_font_face(cr, "serif", CAIRO_FONT_SLANT_NORMAL,
			       CAIRO_FONT_WEIGHT_BOLD);

	cairo_set_font_size(cr, radius * CLOCK_DIGITS_FONTS_SIZE_RATIO);

	cairo_set_line_width(cr, 1.5);

	/*
	 * Workaround to fill space between outer black rim and limit of transparent
	 * mask.
	 */
	cairo_set_source_rgb(cr, 0.0, 0.0, 0.0); /* Black background */
	cairo_rectangle(cr, 0, 0, 64, 64);
	cairo_fill(cr);
	cairo_stroke(cr);

	/* Draw clock background and outer rim */
	cairo_arc(cr, center_x, center_y, radius, 0, 2 * M_PI);
	cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
	cairo_fill_preserve(cr); /* White background */
	cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
	cairo_stroke(cr); /* Black rim */

	/* Draw clock tick marks on outer circle */
	for (digit = 1; digit <= 12; digit++) {
		double xx, yy;
		double text_start_x, text_start_y;
		double first_digit_xoffset = 0;
		char hours[3];
		cairo_text_extents_t extents;
		int divisor;

		sprintf(hours, "%d", digit); /* Convert digit to string */
		hours[1] = 0; /* Make sure to have only first digit */

		/* Get digit dimensions */
		cairo_text_extents(cr, hours, &extents);

		if (digit <= 9) {
			divisor = 2;
		} else {
			divisor = 1;

			/* Manual tweak for '10' and '11' oclock position */
			if (digit == 11)
				first_digit_xoffset =
					ELEVENTH_OCLOCK_FIRST_DIGIT_XOFFSET_RATIO;
			else if (digit == 10)
				first_digit_xoffset =
					TENTH_OCLOCK_FIRST_DIGIT_XOFFSET_RATIO;
		}

		xx = extents.width / divisor + extents.x_bearing;
		yy = extents.height / 2 + extents.y_bearing;

		text_start_x = center_x + radius_numbers * cos(angle) - xx +
			(first_digit_xoffset * radius);
		text_start_y = center_y - radius_numbers * sin(angle) - yy;

		cairo_move_to(cr, text_start_x, text_start_y);
		cairo_show_text(cr, hours);

		if (digit > 9) {
			/* Second digit */
			sprintf(hours, "%d", digit - 10);

			cairo_text_extents(cr, hours, &extents);

			text_start_x += xx - SECOND_DIGIT_XOFFSET;

			cairo_move_to(cr, text_start_x, text_start_y);
			cairo_show_text(cr, hours);
		}

		/* Draw small dot for each digit */
		cairo_arc(cr, center_x + radius_dots * cos(angle),
			  center_y + radius_dots * sin(angle),
			  radius * DIGITS_DOTS_RADIUS_RATIO, 0, 2 * M_PI);
		cairo_fill(cr);

		angle -= M_PI / 6; /* Angle for next digit */
		if (angle < 0)
			angle = 11 * M_PI / 6; /* '4' oclock position */
	}

	cairo_restore(cr);
}

static void
draw_clock_hand_common(GtkWidget *clock, cairo_t *cr, double angle,
		       double len_main_ratio, double len_ext_ratio,
		       double width_ratio)
{
	gint center_x, center_y;
	double sin_x, cos_y;
	double radius, length;

	radius = get_clock_face_radius(clock);

	center_x = gtk_widget_get_allocated_width(clock) / 2;
	center_y = gtk_widget_get_allocated_height(clock) / 2;

	sin_x = sin(angle);
	cos_y = cos(angle);

	cairo_set_line_width(cr, radius * width_ratio);

	length = radius * len_ext_ratio;
	/*
	 * If optional extension len is non null, starting position is at the
	 * outer end of extension. Otherwise, starting position is at center.
	 */
	cairo_move_to(cr, center_x - length * sin_x,
		      center_y - length * -cos_y);
	length = radius * len_main_ratio;
	cairo_line_to(cr, center_x + length * sin_x,
		      center_y + length * -cos_y);

	cairo_stroke(cr);
}

static void
draw_clock_hands(GtkWidget *clock, cairo_t *cr)
{
	gint center_x, center_y;
	double angle = M_PI / 3; /* Starting angle at '12' oclock position */
	double radius;
	int hours, minutes, seconds;
	struct tm *ts;

	ts = clock_get_time();

	center_x = gtk_widget_get_allocated_width(clock) / 2;
	center_y = gtk_widget_get_allocated_height(clock) / 2;

	radius = get_clock_face_radius(clock);

	cairo_save(cr);

	hours = ts->tm_hour;
	minutes = ts->tm_min;
	seconds = ts->tm_sec;

	cairo_set_source_rgb(cr, 0.15, 0.15, 0.15);
	cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);

	/* hour hand:
	 * the hour hand is rotated 30 degrees (pi/6 r) per hour +
	 * 1/2 a degree (pi/360 r) per minute
	 */
	angle = M_PI / 6 * hours + M_PI / 360 * minutes;
	draw_clock_hand_common(clock, cr, angle,
			       HAND_HOURS_LENGTH_RATIO, 0, /* No extension */
			       HAND_HOURS_WIDTH_RATIO);

	/* minute hand:
	 * the minute hand is rotated 6 degrees (pi/30 r) per minute
	 */
	angle = M_PI / 30 * minutes;
	draw_clock_hand_common(clock, cr, angle,
			       HAND_MINUTES_LENGTH_RATIO, 0, /* No extension */
			       HAND_MINUTES_WIDTH_RATIO);

	/* seconds hand:
	 * operates identically to the minute hand
	 */
	if (opts.show_seconds) {
		angle = M_PI / 30 * seconds;
		draw_clock_hand_common(clock, cr, angle,
				       HAND_SECONDS_LENGTH1_RATIO,
				       HAND_SECONDS_LENGTH2_RATIO, /* Draw small
								    * extension
								    */
				       HAND_SECONDS_WIDTH_RATIO);
	}

	/* Draw clock center circle */
	cairo_arc(cr, center_x, center_y,
		  radius * CENTER_CIRCLE_OUTER_RADIUS_RATIO,
		  0, 2 * M_PI);
	cairo_fill(cr);

	/* Draw clock center smaller circle */
	cairo_set_source_rgb(cr, 0.6, 0.6, 0.6);
	cairo_arc(cr, center_x, center_y,
		  radius * CENTER_CIRCLE_INNER_RADIUS_RATIO,
		  0, 2 * M_PI);
	cairo_fill(cr);

	cairo_restore(cr);
}

static void
calendar_display_test(GtkWidget *clock, struct tm *ts, cairo_t *cr,
		      char *format, double y_ratio, double font_ratio)
{
	cairo_text_extents_t extents;
	double x;
	gint center_x, start_y;
	double diameter;
	char str[32];

	diameter = 2 * get_clock_face_radius(clock);
	center_x = (gtk_widget_get_allocated_width(clock) / 2) - 2;
	start_y = (gtk_widget_get_allocated_height(clock) / 2) - (diameter / 2);

	cairo_set_font_size(cr, diameter * font_ratio);

	/* Obtain ASCII reprentation of time format specified. */
	strftime(str, 256, format, ts);

	/* Get text dimensions */
	cairo_text_extents(cr, str, &extents);
	x = center_x - extents.width / 2;
	cairo_move_to(cr, x, start_y + (diameter * y_ratio));
	cairo_show_text(cr, str);
}

static void
draw_calendar(GtkWidget *clock, cairo_t *cr)
{
	struct tm *ts;

	cairo_save(cr);

	cairo_set_source_rgb(cr, 1.0, 1.0, 1.0); /* White background */
	cairo_rectangle(cr, 0, 0, 64, 64);
	cairo_fill(cr);
	cairo_stroke(cr);

	cairo_select_font_face(cr, "sans-serif", CAIRO_FONT_SLANT_NORMAL,
			       CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_source_rgb(cr, 0.0, 0.0, 0.0); /* Black text */

	ts = clock_get_time();

	/* Weekday */
	calendar_display_test(clock, ts, cr, "%A",
			      CALENDAR_WEEKDAY_Y_POSITION_RATIO,
			      CALENDAR_TEXT_FONTS_SIZE_RATIO);

	/* Month */
	calendar_display_test(clock, ts, cr, "%B",
			      CALENDAR_MONTH_Y_POSITION_RATIO,
			      CALENDAR_TEXT_FONTS_SIZE_RATIO);

	/* Day of month */
	/* Minus sign in format -> No leading zero */
	calendar_display_test(clock, ts, cr, "%-d",
			      CALENDAR_DAYOFMONTH_Y_POSITION_RATIO,
			      CALENDAR_DIGITS_FONTS_SIZE_RATIO);

	cairo_restore(cr);
}

static gboolean
#if defined (HAVE_GTK2)
hvclock_expose(GtkWidget *clock, GdkEventExpose *event)
#elif defined (HAVE_GTK3)
draw_handler(GtkWidget *clock, cairo_t *cr, gpointer d G_GNUC_UNUSED)
#endif
{
#if defined (HAVE_GTK2)
	cairo_t *cr;
#elif defined (HAVE_GTK3)
	GdkRectangle rect;
#endif

#if defined (HAVE_GTK2)
	/* Get a cairo_t */
	cr = gdk_cairo_create(gtk_widget_get_window(clock));

	cairo_rectangle(cr, event->area.x, event->area.y,
		event->area.width, event->area.height);
#elif defined (HAVE_GTK3)
	gdk_cairo_get_clip_rectangle(cr, &rect);
        if (opts.debug)
        {
            printf("Redrawing (%d,%d+%d+%d)\n", rect.x, rect.y,
                   rect.width, rect.height);
        }

	cairo_rectangle(cr, rect.x, rect.y, rect.width, rect.height);
	cairo_clip(cr);
#endif

	if (hvclock_mode == CLOCK_MODE) {
		dockapp_set_mask(clock, clock_mask_xpm);
		draw_clock_background(clock, cr);
		draw_clock_hands(clock, cr);
	} else {
		dockapp_set_mask(clock, calendar_mask_xpm);
		draw_calendar(clock, cr);
	}

#if defined (HAVE_GTK2)
	cairo_destroy(cr);
#endif

	return TRUE;
}

static void
hvclock_redraw_canvas(GtkWidget *widget)
{
#if defined (HAVE_GTK2)
	GdkRegion *region;
#elif defined (HAVE_GTK3)
	cairo_region_t *region;
#endif

	if (!gtk_widget_get_window(widget))
		return;

#if defined (HAVE_GTK2)
	region = gdk_drawable_get_clip_region(gtk_widget_get_window(widget));
#elif defined (HAVE_GTK3)
	region = gdk_window_get_clip_region(gtk_widget_get_window(widget));
#endif

	/* redraw the cairo canvas completely by exposing it */
	gdk_window_invalidate_region(gtk_widget_get_window(widget), region, TRUE);
	gdk_window_process_updates(gtk_widget_get_window(widget), TRUE);

#if defined (HAVE_GTK2)
	gdk_region_destroy(region);
#elif defined (HAVE_GTK3)
	cairo_region_destroy(region);
#endif
}

static gboolean
hvclock_button_release(GtkWidget *clock, GdkEventButton *event)
{
	(void) event; /* Unused parameter. */

	if (opts.debug)
		printf("%s: button released\n", PACKAGE_NAME);

	/* single-click --> changing viewing mode */
	if (hvclock_mode == CLOCK_MODE) {
		hvclock_mode = CALENDAR_MODE;
		if (opts.debug)
			printf("%s: switching to calendar mode\n", PACKAGE_NAME);
	} else {
		hvclock_mode = CLOCK_MODE;
		if (opts.debug)
			printf("%s: switching to clock mode\n", PACKAGE_NAME);
	}

	/* Update clock/calendar... */
	hvclock_redraw_canvas(clock);

	return FALSE;
}

static gboolean
hvclock_update(gpointer data)
{
	GtkWidget *clock = GTK_WIDGET(data);
	static struct tm previous;
	struct tm *now;
	int update = false;

	now = clock_get_time();

	/* Update only if time has changed. If the hours or minutes have
	 * changed, force update. */
	if ((now->tm_hour != previous.tm_hour) ||
	    (now->tm_min != previous.tm_min)) {
		update = true;
	}

	/* If the seconds have changed, force update only if displaying of
	 * the seconds hand is requested. */
	if (opts.show_seconds && (now->tm_sec != previous.tm_sec)) {
		update = true;
	}

	if (update) {
		hvclock_redraw_canvas(clock);
		previous = *now;
	}

	return TRUE; /* keep running this event */
}

void
hvclock_init(GtkWidget *win)
{
	gtk_widget_add_events(win, GDK_BUTTON_PRESS_MASK |
			      GDK_BUTTON_RELEASE_MASK |
			      GDK_POINTER_MOTION_MASK);

#if defined (HAVE_GTK2)
	g_signal_connect(G_OBJECT(win), "expose-event",
			 G_CALLBACK(hvclock_expose), NULL);
#elif defined (HAVE_GTK3)
	g_signal_connect(G_OBJECT(win), "draw",
			 G_CALLBACK(draw_handler), NULL);
#endif
	g_signal_connect(G_OBJECT(win), "button-press-event",
			 G_CALLBACK(hvclock_button_release), NULL);
	g_signal_connect(G_OBJECT(win), "button-release-event",
			 G_CALLBACK(hvclock_button_release), NULL);

	/* update the clock once a second */
	g_timeout_add(1000, hvclock_update, win);

	hvclock_redraw_canvas(win);
}
