/*
 * options.h
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef OPTIONS_H
#define OPTIONS_H 1

struct opts_t
{
    int debug;
    int show_seconds;
};

/* Exported variables */
#undef _SCOPE_
#ifdef OPTIONS_M
#  define _SCOPE_ /**/
#else
#  define _SCOPE_ extern
#endif

_SCOPE_ struct opts_t opts;

void
parse_command_line_options(int argc, char *argv[]);

#endif /* OPTIONS_H */
