/*
 * clock.h
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef CLOCK_H
#define CLOCK_H 1

#include <gtk/gtk.h>

void
hvclock_init(GtkWidget *win);

#endif /* CLOCK_H */
